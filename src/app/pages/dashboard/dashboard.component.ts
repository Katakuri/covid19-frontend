import { Component, OnInit } from '@angular/core';
import Chart from 'chart.js';
import {CallapiService} from '../../service/callapi.service';

// core components
import {
  chartOptions,
  parseOptions,
  chartExample1,
  chartExample2
} from "../../variables/charts";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public datasets: any;
  public data: any;
  public salesChart;
  public clicked: boolean = true;
  public clicked1: boolean = false;
  public nbCases;
  public nbDeaths;
  public deathsByCountry;
  public countryMoreCases;
  public countryLessCases;
  public deathsCasesByMounth;
  public deathsByMouth = [];
  public casesByMouth = [];

  constructor(
    private apiService : CallapiService
  ) { }

  ngOnInit() {
    this.apiService.getCaseInWorld().subscribe((res) => {
      console.log(res)
      this.nbCases = res[0]._fields[0].low;
      this.nbDeaths = res[0]._fields[1].low; 
    }, (err) => console.log(err));

    this.apiService.getDeathsByCoutry().subscribe((res) => {
      //Récuperation des cas et décés par pays
      this.deathsByCountry = res;
      //Pays avec le plus de cas premier dans le tableau car ordonné par ordre décroissant
      this.countryMoreCases = res[0];
      // Dernier element du tableau pour le pays avec le mois de décés
      this.countryLessCases = res[this.deathsByCountry.length-1];
    }, (err) => console.log(err));

     this.apiService.getDeathsAndCasesByMounth().subscribe((res) => {
      console.log(res);
      this.deathsCasesByMounth = res;
      this.deathsCasesByMounth.forEach(mouth => {
        this.casesByMouth.push(mouth._fields[2].low);
        this.deathsByMouth.push(mouth._fields[1].low);
      });
      this.casesByMouth.pop();
      this.deathsByMouth.pop();
      console.log(chartExample2.data.datasets[0].data);
      chartExample2.data.datasets[0].data = this.deathsByMouth;
      chartExample1.data.datasets[0].data = this.casesByMouth;
     console.log(chartExample2.data.datasets[0].data) ;

     var chartOrders = document.getElementById('chart-orders');

     parseOptions(Chart, chartOptions());
 
 
     var ordersChart = new Chart(chartOrders, {
       type: 'bar',
       options: chartExample2.options,
       data: chartExample2.data
     });
 
     var chartSales = document.getElementById('chart-sales');
 
     this.salesChart = new Chart(chartSales, {
       type: 'line',
       options: chartExample1.options,
       data: chartExample1.data
     });

    }, (err) => console.log(err));
  }



  public getColorCases(nbCases : Number){
    return nbCases > 50000;
  }

  public getColorsDeaths(nbDeaths : Number){
    return nbDeaths > 5000;
  }

  public flagCountry(country){
    var coutryCode = country._fields[1].toLowerCase();
    return 'flag-icon flag-icon-'+coutryCode
  }

}
