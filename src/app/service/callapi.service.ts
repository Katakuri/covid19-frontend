import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CallapiService {

  constructor(
    private http : HttpClient
  ) {}

  url = 'http://localhost:3000/';

  getCaseInWorld(){
    return this.http.get(this.url+'casesDeathsInWorld/');
  }

  getDeathsByCoutry(){
    return this.http.get(this.url+'deathsByCoutry');
  }

  getDeathsAndCasesByMounth(){
    return this.http.get(this.url+'deathsByMounth');
  }
}
